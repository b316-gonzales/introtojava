package com.zuitt;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        // Instantiate a new User object using the User class's parameterized constructor
        User user = new User("Terence Gaffud", 25, "tgaff@mail.com", "Quezon City");

        // Instantiate a new Course object using the default constructor of the Course class
        Course course = new Course();

        // Use the setters of the Course class to initialize every instance variable of the new course
        course.setName("MACQ004");
        course.setDescription("An introduction to Java for career-shifters");
        course.setSeats(30);
        course.setFee(0.00);
        // Set the startDate and endDate using appropriate Date objects
        course.setStartDate(LocalDate.now());
        course.setEndDate(LocalDate.now());

        // Set the instructor of the course
        course.setInstructor(user);

        // Output the coherent message
        System.out.println("Hi! I'm " + user.getName() + ". I'm " + user.getAge() + " years old.");
        System.out.println("You can reach me via my email: " + user.getEmail() + ".");
        System.out.println("When I'm off work, I can be found at my house in " + user.getAddress() + ".");
        System.out.println();
        System.out.println("Welcome to the course " + course.getName() + ".");
        System.out.println("This course can be described as " + course.getDescription() + ".");
        System.out.println("Your instructor for this course is Sir " + course.getInstructor().getName() + ".");
        System.out.println("Enjoy!");
    }
}
