package com.zuitt;
import java.time.LocalDate;

public class Course {

    private String name;
    private String description;
    private int seats;
    private double fee;
    private LocalDate startDate;
    private LocalDate endDate;
    private User instructor;

    // Default constructor
    public Course() {
    }

    // Parameterized constructor
    public Course(String name, String description, int seats, double fee, LocalDate startDate, LocalDate endDate, User instructor) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }

    // Getters and setters for the instance variables
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDateParam) {
        this.startDate = startDateParam;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDateParam) {
        this.endDate = endDateParam;
    }

    public User getInstructor() {
        return instructor;
    }

    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }


}
