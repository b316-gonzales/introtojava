package com.zuitt;
import java.util.Scanner;

public class Activity_WhileLoop {
    public static void main(String[] args) {
        // Output a static message asking for an integer
        System.out.println("Input an integer whose factorial will be computed:");

        // Instantiate a Scanner object
        Scanner input = new Scanner(System.in);

        try {
            // Receive the next console input of data type int
            int num = input.nextInt();

            // Declare and initialize variables
            int answer = 1;
            int counter = 1;

            // Check if the number is zero or negative
            if (num <= 0) {
                System.out.println("Invalid input. The number should be a positive integer.");
            } else {
                // Calculate the factorial using a while loop
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }

                // Output the result
                System.out.println("The factorial of " + num + " is " + answer);
            }
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter a valid integer.");
        }

    }

}
