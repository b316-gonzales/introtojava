package com.zuitt;

public class StretchGoal {
    public static void main(String[] args) {
        int rows = 5; // Number of rows in the pattern

        // Outer loop
        for (int i = 1; i <= rows; i++) {
            // Inner loop
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            System.out.println(); // Move to the next line after each row
        }
    }
}
