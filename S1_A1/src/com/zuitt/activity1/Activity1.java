package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        // Create a new Scanner object
        Scanner scanner = new Scanner(System.in);

        // Declare variables to store user's information.
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        // Prompt the user to enter their information
        System.out.println("First Name: ");
        firstName = scanner.nextLine();

        System.out.println("Last Name: ");
        lastName = scanner.nextLine();

        System.out.println("First Subject Grade: ");
        firstSubject = scanner.nextDouble();

        System.out.println("Second Subject Grade: ");
        secondSubject = scanner.nextDouble();

        System.out.println("Third Subject Grade: ");
        thirdSubject = scanner.nextDouble();

        // Calculate the average grade
        double averageGrade = (firstSubject + secondSubject + thirdSubject) / 3;

        // Remove decimal point in average
        int averageGradeInt = (int) averageGrade;

        // Display the user's full name and average grade.
        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + averageGradeInt);
    }
}
