package com.zuitt;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe");
        contact1.addContactNumber("+639152468596");
        contact1.addContactNumber("+639228547963");
        contact1.addAddress("my home in Quezon City");
        contact1.addAddress("my office in Makati City");

        Contact contact2 = new Contact("Jane Doe");
        contact2.addContactNumber("+639162148573");
        contact2.addContactNumber("+639173698541");
        contact2.addAddress("my home in Caloocan City");
        contact2.addAddress("my office in Pasay City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        ArrayList<Contact> contacts = phonebook.getContacts();

        if (contacts.isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String contactNumber : contact.getContactNumbers()) {
                    System.out.println(contactNumber);
                }
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                for (String address : contact.getAddresses()) {
                    System.out.println(address);
                }
                System.out.println("=================================");
            }
        }
    }
}
