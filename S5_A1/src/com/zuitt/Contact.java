package com.zuitt;
import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> contactNumbers;
    private ArrayList<String> addresses;

    public Contact() {
        this.contactNumbers = new ArrayList<>();
        this.addresses = new ArrayList<>();
    }

    public Contact(String name) {
        this.name = name;
        this.contactNumbers = new ArrayList<>();
        this.addresses = new ArrayList<>();
    }

    public void addContactNumber(String contactNumber) {
        this.contactNumbers.add(contactNumber);
    }

    public void addAddress(String address) {
        this.addresses.add(address);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(ArrayList<String> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<String> addresses) {
        this.addresses = addresses;
    }
}

