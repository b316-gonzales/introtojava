package com.zuitt;
import java.util.ArrayList;
import java.util.HashMap;

public class S2_A2 {
    public static void main(String[] args) {
        // Declare an array for containing the first 5 prime numbers
        int[] primeNumbers = new int[5];

        // Assign the first 5 prime numbers to their respective indices
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        // Output specific elements of the array
        System.out.println("The first prime number is: " + primeNumbers[0]);


        // Create an ArrayList of String elements using generics
        ArrayList<String> friends = new ArrayList<>();

        // Add elements to the ArrayList
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        // Output the contents of the ArrayList
        System.out.println("My friends are: " + friends);


        // Create a HashMap of keys with data type String and values with data type Integer using generics
        HashMap<String, Integer> inventory = new HashMap<>();

        // Add key-value pairs to the HashMap
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        // Output the contents of the HashMap
        System.out.println("Our current inventory consists of: " + inventory);
    }
}
